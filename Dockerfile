FROM python:3.8-slim-bullseye

RUN useradd -ms /bin/bash watchdog
USER watchdog
WORKDIR /home/watchdog
ENV PATH "$PATH:/home/watchdog/.local/bin"

ADD src/* ./
ADD requirements.txt .
RUN pip install -r requirements.txt

CMD ["python", "-u", "Watchdog.py"]`
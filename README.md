# Watchdog
## Build Docker image
### DockerHub
```commandline
docker build -t kcpr/watchdog:0.0.1 .
docker tag kcpr/watchdog:0.0.1 kcpr/watchdog:latest

docker login docker.io
docker push kcpr/watchdog
```
### GitLab
```commandline
docker build -t registry.gitlab.com/kkasztel/watchdog:0.0.1 .
docker tag registry.gitlab.com/kkasztel/watchdog:0.0.1 registry.gitlab.com/kkasztel/watchdog:latest

docker login registry.gitlab.com
docker push registry.gitlab.com/kkasztel/watchdog
```

## Run
- Copy `.env-template` file as `.env` and fill required properties
- Execute command: `docker-compose up -d`
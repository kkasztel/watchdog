from requests import get


class Notifier:
    def __init__(self, token: str, bot_id: str):
        self.token = token
        self.bot_id = bot_id

    def notify(self, msg: str):
        url = f'https://api.telegram.org/bot{self.token}'
        params = {'chat_id': self.bot_id, 'text': msg}
        get(f'{url}/sendMessage', params=params)

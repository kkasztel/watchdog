import requests
import ssl

class RequestFetcher:
    def __init__(self):
        self.session = requests.session()
        self.session.mount('https://', TLSAdapter())

    def fetch(self, url: str) -> str:
        response = self.session.get(url)
        if response.ok:
            return str(response.content)
        else:
            print(f'Error {response.status_code}')
            return ''

class TLSAdapter(requests.adapters.HTTPAdapter):

    def init_poolmanager(self, *args, **kwargs):
        ctx = ssl.create_default_context()
        ctx.set_ciphers('DEFAULT@SECLEVEL=1')
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE
        kwargs['ssl_context'] = ctx
        return super(TLSAdapter, self).init_poolmanager(*args, **kwargs)



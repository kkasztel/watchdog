import os
from pathlib import Path

from selenium import webdriver

from utils import get_current_time_millis


class SeleniumFetcher:
    def __init__(self, selenium_url: str, screenshot_enabled: bool, screenshot_path: str):
        self.selenium_url = selenium_url
        self.screenshot_enabled = screenshot_enabled
        self.screenshot_path = screenshot_path
        self.opts = webdriver.ChromeOptions()
        self.opts.add_argument('--disable-blink-features=AutomationControlled')
        self.opts.add_argument("--disable-dev-shm-usage")
        self.opts.add_experimental_option("excludeSwitches", ["enable-automation"])
        self.opts.add_experimental_option('useAutomationExtension', False)
        self.opts.add_argument(
            "user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.64 Safari/537.36")

    def fetch(self, url: str) -> str:
        driver = self.setup_driver()
        driver.get(url)
        if self.screenshot_enabled:
            driver.save_screenshot(self.screenshot_name())
        content = driver.page_source
        self.close_driver(driver)
        return content

    def setup_driver(self):
        driver = webdriver.Remote(command_executor=self.selenium_url, options=self.opts)
        driver.set_window_position(0, 0)
        driver.set_window_size(2560, 1440)
        driver.implicitly_wait(2)
        return driver

    def close_driver(self, driver):
        driver.close()
        driver.quit()

    def screenshot_name(self) -> str:
        screenshot_dir = Path(os.path.expanduser(self.screenshot_path))
        screenshot_dir.mkdir(parents=True, exist_ok=True)
        return os.path.normpath(f'{screenshot_dir}/s-{get_current_time_millis()}.png')

import datetime
import json
import os
import time

import pytz
import schedule

from Notifier import Notifier
from RequestFetcher import RequestFetcher
from SeleniumFetcher import SeleniumFetcher
from utils import debouncer


class Watchdog:

    def __init__(self):
        self.start_notification_sent = False
        self.throttle_msg = 30
        self.time_format = '%H:%M'
        self.time_zone = 'Europe/Warsaw'
        self.url = 'https://sklep.tauron.pl'
        self.fetcher_type = os.environ.get('FETCHER_TYPE', 'request')
        self.selenium_url = os.environ.get('SELENIUM_URL', 'http://localhost:4444/wd/hub')
        self.screenshot_enabled = os.environ.get('SCREENSHOT_ENABLED', 'True').lower() == 'true'
        self.screenshot_path = os.environ.get('SCREENSHOT_PATH', '~/watchdog')
        self.notifier = Notifier(os.environ['TELEGRAM_TOKEN'], os.environ['TELEGRAM_BOT_ID'])
        self.notify = debouncer(self.notify_available, self.throttle_msg * 1000)
        self.weekdays = json.loads(os.environ.get('WEEKDAYS', '[1, 2, 4]'))
        self.start_time = self.parse_time(os.environ.get('START_TIME', '10:50'))
        self.end_time = self.parse_time(os.environ.get('END_TIME', '12:30'))
        if self.fetcher_type == 'request':
            print('Setting up RequestFetcher')
            self.fetcher = RequestFetcher()
            self.loop_time = 5
        else:
            print('Setting up SeleniumFetcher')
            self.fetcher = SeleniumFetcher(self.selenium_url, self.screenshot_enabled, self.screenshot_path)
            self.loop_time = 3

    def notify_available(self):
        self.do_notify('Available!')

    def do_notify(self, msg: str):
        print(msg)
        self.notifier.notify(msg)

    def check(self):
        content = self.fetcher.fetch(self.url)
        if 'JARET' in content and ('Zamów' in content or content.count('Produkt czasowo') < 5):
            self.notify()
        else:
            print('Not available')

    def job(self):
        now = self.now()
        if now.isoweekday() in self.weekdays and self.start_time < now.time() < self.end_time:
            if not self.start_notification_sent:
                self.do_notify('Checking started!')
                self.start_notification_sent = True
            print(f'Checking at: {now.isoformat()}')
            self.check()
        elif self.start_notification_sent:
            self.do_notify('Checking finished!')
            self.start_notification_sent = False

    def parse_time(self, time_str: str) -> datetime.time:
        return datetime.datetime.strptime(time_str, self.time_format).time()

    def now(self) -> datetime:
        return datetime.datetime.now(tz=pytz.timezone(self.time_zone))

    def config(self) -> dict:
        return {
            'START_TIME': self.start_time.__str__(),
            'END_TIME': self.end_time.__str__(),
            'WEEKDAYS': self.weekdays
        }


if __name__ == '__main__':
    w = Watchdog()
    print(f'Program running: {w.now().isoformat()}')
    print(w.config())

    schedule.every(w.loop_time).seconds.do(w.job)
    while 1:
        schedule.run_pending()
        time.sleep(1)

import time


def debouncer(callback, throttle_time_limit=100):
    last_millis = 0

    def throttle():
        nonlocal last_millis
        curr_millis = get_current_time_millis()
        if (curr_millis - last_millis) > throttle_time_limit:
            last_millis = get_current_time_millis()
            callback()

    return throttle


def get_current_time_millis():
    return int(round(time.time() * 1000))
